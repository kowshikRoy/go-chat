package trace

import (
	"bytes"
	"testing"
)

func TestNew(t *testing.T) {
	var buf bytes.Buffer
	tracer := New(&buf)
	if tracer == nil {
		t.Error("Return from New shouldn't be nil")
	} else {
		tracer.Trace("Hello Trace Package.")
		if buf.String() != "Hello Trace Package.\n" {
			t.Errorf("Trace shouldn't write '%s'", buf.String())
		}
	}
}

func TestOff(t *testing.T) {
	silentTracer := Off()
	silentTracer.Trace("something")
}
