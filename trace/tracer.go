package trace

import (
	"fmt"
	"io"
)

// Traces is an interface that describes an object capable of
// tracing events througout code
type Tracer interface {
	Trace(...interface{})
}

// New creates a new Tracer that will write output to
// the specified io.Writer
func New(w io.Writer) Tracer {
	return &tracer{out: w}
}

// tracer is a Tracer that writes output to
// io.Writer
type tracer struct {
	out io.Writer
}

// Trace writes the arguments to this Tracers io.Writer.
func (t *tracer) Trace(a ...interface{}) {
	fmt.Fprint(t.out, a...)
	fmt.Fprintln(t.out)
}

// nilTracer
type nilTracer struct{}

// Trace for a nilTraces does nothing.
func (t *nilTracer) Trace(a ...interface{}) {}

// Off creates a Tracer which ignores call to Trace
func Off() Tracer {
	return &nilTracer{}
}
