package main

import (
	"log"
	"net/http"

	"github.com/kowshikRoy/go-chat/trace"

	"github.com/gorilla/websocket"
)

const (
	socketBufferSize  = 1024
	messageBufferSize = 256
)

// room represents a room where user chat's in
type room struct {

	// forward is a channel that holds the incoming messages
	// that sould be forwared to other clients
	forward chan []byte

	// join is a channel that holds client who wants to join the room
	join chan *client

	// leave is a channel that holds client who want to leave
	leave chan *client

	// clients holds all the current client in the room
	clients map[*client]bool

	// tracer will receive trace information of activity
	tracer trace.Tracer
}

// newRoom returns a room which is ready to go
func newRoom() (r *room) {
	r = &room{
		forward: make(chan []byte),
		join:    make(chan *client),
		leave:   make(chan *client),
		clients: make(map[*client]bool),
		tracer:  trace.Off(),
	}
	return
}

func (r *room) run() {
	for {
		select {
		case client := <-r.join:
			// joining
			r.clients[client] = true
			r.tracer.Trace("New client joined")
		case client := <-r.leave:
			// leaving
			delete(r.clients, client)
			close(client.send)
			r.tracer.Trace("Client left")
		case msg := <-r.forward:
			r.tracer.Trace("Message received: ", string(msg))
			// forward msg to all clients
			for client := range r.clients {
				client.send <- msg
				r.tracer.Trace(" -- sent to client")
			}
		}
	}
}

var upgrader = websocket.Upgrader{ReadBufferSize: messageBufferSize, WriteBufferSize: socketBufferSize}

func (r *room) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	socket, err := upgrader.Upgrade(w, req, nil)
	if err != nil {
		log.Fatal("ServeHTTP:", err)
		return
	}
	client := &client{
		socket: socket,
		send:   make(chan []byte, messageBufferSize),
		room:   r,
	}
	r.join <- client
	defer func() { r.leave <- client }()
	go client.write()
	client.read()
}
