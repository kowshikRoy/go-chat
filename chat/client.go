package main

import "github.com/gorilla/websocket"

// client represents an active user connected to chat server
type client struct {

	// socket is the websocket for this client
	socket *websocket.Conn

	// send is a channel through which received messages are queued
	// and forwared to the user's browsers
	send chan []byte

	// room is the room this user is chatting in
	room *room
}

// reads from client socket and forwards to room channel
func (c *client) read() {
	defer c.socket.Close()
	for {
		_, msg, err := c.socket.ReadMessage()
		if err != nil {
			return
		}
		c.room.forward <- msg
	}
}

// writes to client socket
func (c *client) write() {
	defer c.socket.Close()
	for msg := range c.send {
		err := c.socket.WriteMessage(websocket.TextMessage, msg)
		if err != nil {
			return
		}
	}
}
