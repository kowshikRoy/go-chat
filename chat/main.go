package main

import (
	"flag"
	"html/template"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"sync"

	"github.com/kowshikRoy/go-chat/trace"
)

// templ represents a single template
type templateHandler struct {
	once     sync.Once
	fileName string
	templ    *template.Template
}

// ServeHTTP handles the HTTP request.
func (t *templateHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	t.once.Do(func() {
		t.templ = template.Must(template.ParseFiles(filepath.Join("templates", t.fileName)))
	})
	t.templ.Execute(w, r)
}
func main() {
	var addr = flag.String("addr", ":8080", "The addr of the application")
	flag.Parse() //parse the flag

	r := newRoom()
	r.tracer = trace.New(os.Stdout)

	//root
	http.Handle("/", &templateHandler{fileName: "chat.html"})
	http.Handle("/room", r)

	//get the room running
	go r.run()

	//start the server
	log.Println("Starting the server on", *addr)
	if err := http.ListenAndServe(*addr, nil); err != nil {
		log.Fatal("ListenAndServe:", err)
	}
}
